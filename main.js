/* Begin Scripts Sonova */
$(document).ready(function () {
// Scroll to top
(function () {
    var scrollToTop = $('.j-scroll-top');
    scrollToTop.click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 500);
    });
})();

// Anchor slide effect with offset
(function () {
    var $anchorLinks = $('a[href^="#"]').not(".j-tab");

    // Set offset for
    var offset = 114; // header + 30px

    if ($(window).width() < 768) {
        offset = 66; // header + 20px
    }

    // Event click listener
    $anchorLinks.click(function (event) {
        event.preventDefault();
        // Scroll to position - offset
        $('html, body').animate({
            scrollTop: $($(this).attr("href")).offset().top - offset
        }, 500);
    });

})();

// Survey form submit
(function () {
  $('#survey_submit').on('click', function () {
    var url = $(this).data('url');
    var code = $('#survey_code').val();
    window.location.href = url + '?c=' + code;
  });
})();

// Header functions
$(function () {
  var togglerNav = document.querySelector(".j-toggle-nav");
  var toggleMobileSearch = document.querySelector(".j-mobile-search-toggle");
  var mobileSearch = document.querySelector(".j-mobile-search");
  var header = document.querySelector(".j-header");
  var headerMobile = document.querySelector(".j-header-mobile");
  var bodyElem = document.querySelector("body");
  var $toggleSubmenuLink = $('.j-toggle-mob-submenu');
  var owlMenu = $('.j-carousel-menu');
  var toggleSearch = $(".j-search-toggle");
  var search = $(".j-search");
  var flyoutCloseTimer;
  var flyoutOpenTimer;

  // Flyout anchor iPad fix
  $(".j-flyout a").on("click touchend", function (e) {
    var el = $(this);
    var link = el.attr("href");
    window.location = link;
  });

  // Window scroll listener doing magic with header
  if (header) {
    window.onscroll = headerChange;
  }

  // Open mobile menu
  if (togglerNav && headerMobile) {
	$(".j-toggle-nav").on("click", function (e) {
		toggleNav();
	});
  }

  // Open mobile search
  if (toggleMobileSearch && mobileSearch) {
	$(".j-mobile-search-toggle").on("click", function (e) {
		toggleMobSearch();
	});
  }

  // Header opacity on scroll
  function headerChange() {
    var currentScroll = window.pageYOffset | document.body.scrollTop;
    var offset = 80; // Set offset from top

    if (currentScroll > offset) {
      header.classList.add("scroll");
      headerMobile.classList.add("scroll");
    } else {
      header.classList.remove("scroll");
      headerMobile.classList.remove("scroll");
    }
  }

  function toggleNav() {

    if (bodyElem.classList.contains("search-opened")) {
      headerMobile.classList.remove("search-opened");
      bodyElem.classList.remove("search-opened");
    }

    headerMobile.classList.toggle("nav-opened");
    bodyElem.classList.toggle("nav-opened");
  }

  function toggleMobSearch() {

    if (bodyElem.classList.contains("nav-opened")) {
      headerMobile.classList.remove("nav-opened");
      bodyElem.classList.remove("nav-opened");
    }

    headerMobile.classList.toggle("search-opened");
    bodyElem.classList.toggle("search-opened");
  }

  // MouseOver Link in menu
  $(".j-flyout-link")
    .mouseout(CloseFlyouts)
    .mouseover(function (e) {
      clearTimeout(flyoutCloseTimer);
      clearTimeout(flyoutOpenTimer);
      flyoutOpenTimer = setTimeout(function () {
        // first hide notused
        $(".j-flyout-link").removeClass("active");
        $(".j-flyout").removeClass("active");
        // then open current
        var currentFlyoutElem = $(e.target);
        currentFlyoutElem.addClass("active");
        var currentFlyout = "." + currentFlyoutElem.data("flyout");
        $(currentFlyout).addClass("active");
      }, 200);
    });

  // MouseOver Flyout
  $(".j-flyout")
    .mouseout(CloseFlyouts)
    .mouseover(function (e) {
      clearTimeout(flyoutOpenTimer);
      clearTimeout(flyoutCloseTimer);
    });

  function CloseFlyouts() {
    clearTimeout(flyoutOpenTimer);
    flyoutCloseTimer = setTimeout(function () {
      $(".j-flyout-link").removeClass("active");
      $(".j-flyout").removeClass("active");
    }, 100);
  }

  // Toggle Mobile Submenu
  $toggleSubmenuLink.click(function () {
    $(this).closest('li').toggleClass('active');
  });

  // Carousel mobile navigation
  owlMenu.owlCarousel({
    loop: false,
    nav: false,
    dots: true,
    autoplay: false,
    items: 1,
  });

  // Search
  toggleSearch.click(function () {
    console.log("Opening search");
    openSearch();
  });
  function openSearch() {
    search.toggleClass("active");
    toggleSearch.toggleClass("active");
  }

});
// Toggle mobile footer menu
(function () {
    var $footerMenuLink = $('.j-footer .j-footer-menu-toggle');
    // Event click
    $footerMenuLink.click(function () {
        $(this).closest('.j-footer-menu').toggleClass('active');
    });
})();
// Kentico Generated Form Submit button
// Version: 1.0
// Author: Michal Měcháček; Sprinx Systems a.s.
(function () {
    var submitButton = document.querySelectorAll(".sx-form .FormButton");

    for (i = 0; i < submitButton.length; i++) {
        // Get settings
        var currSubmitButton = submitButton[i];
        var currParent = currSubmitButton.parentElement;
        var buttonWrapper = document.createElement("span");
        buttonWrapper.classList.add("FormButtonWrapper");

        // Structure generation
        currParent.appendChild(buttonWrapper);
        buttonWrapper.appendChild(currSubmitButton);
    }
})();

/* Disable button to prevent double submit */
$(document).ready(function () {
  $("div.sx-form input.FormButton").click(function () {
    var $submitBtn = $(this);
    $submitBtn.click(function () { return false; });
    $submitBtn.attr('onclick', 'return false;');
    $submitBtn.addClass("btn-disabled");
    $submitBtn.attr("value", RS.formSending);
    return true;
  });
});
// Kentico Generated select field DOM Update
// Version: 1.0
// Author: Michal Měcháček; Sprinx Systems a.s.
(function () {
  var dropDownLists = document.querySelectorAll(".sx-form .DropDownField");

  for (i = 0; i < dropDownLists.length; i++) {
    // Get settings
    var currDropDown = dropDownLists[i];
    var currParent = currDropDown.parentElement;
    var dropDownWrapper = document.createElement("span");
    dropDownWrapper.classList.add("DropDownListControl");

    // Structure generation
    currParent.appendChild(dropDownWrapper);
    dropDownWrapper.appendChild(currDropDown);
  }

})();
// Kentico Generated upload DOM update
// Version: 1.0
// Author: Michal Měcháček; Sprinx Systems a.s.
(function () {
  var uploadFields = document.querySelectorAll(".sx-form .uploader-upload");

  for (var i = 0; i < uploadFields.length; i++) {
    
    // Get elements
    var uploadFileText = document.createElement("span");
    var uploadInput = uploadFields[i].querySelector(".uploader-input-file");
    var uploadButton = document.createElement("label");

    // CSS Classes
    uploadFileText.classList.add("uploader-text");
    uploadButton.classList.add("uploader-button");
    uploadButton.htmlFor = uploadInput.id;

    // Localization of button and default text field
    // Don't forget add global var ResourceStrings object with Localization
    if (typeof ResourceStrings !== "undefined") {
      uploadButton.innerHTML = ResourceStrings.FileInputButton;
      uploadFileText.innerHTML = ResourceStrings.FileInputField;
    } else {
      uploadButton.innerHTML = "Browse...";
      uploadFileText.innerHTML = "No file selected.";
    }

    // Update container structure
    uploadFields[i].appendChild(uploadFileText);
    uploadFields[i].appendChild(uploadButton);

    // Add event handler for update the text
    uploadInput.addEventListener("change", function () {
      processFileNames(this);
    });

  }

  function processFileNames(fileInput) {
    var files = fileInput.files;
    var str = "";

    for (var file = 0; file < files.length; file++) {
      str += files[file].name;
      if (files.length > 1) {
        str += "; ";
      }
    }

    var textLabel = fileInput.parentElement.querySelector(".uploader-text");
    textLabel.innerHTML = str;
  }

})();
// Handle tracking fields
// Author: Cosmin Ivan, Futurecom
(function () {

    hideFields();
    populateFieldsFromQueryString();

    function hideFields() {
        $(".son_tracking_field").hide();
    }

    function populateFieldsFromQueryString() {
        var qs = parseQueryString();
        populateFieldFromQueryString(".son_form_source", qs.source);
        populateFieldFromQueryString(".son_form_info", qs.info);
        populateFieldFromQueryString(".son_form_utm_campaign", qs.utmCampaign);
        populateFieldFromQueryString(".son_form_tag", qs.tag);
        populateFieldFromQueryString(".son_form_optin_email", qs.optin_email);
        populateFieldFromQueryString(".son_form_optin_sms", qs.optin_sms);
        populateFieldFromQueryString(".son_form_preferred_store", qs.preferredStore);
        populateFieldFromQueryString(".son_form_ht_conducted", qs.htConducted);
        populateFieldFromQueryString(".son_form_utm_medium", qs.utmMedium);
        populateFieldFromQueryString(".son_form_utm_source", qs.utmSource);
        populateFieldFromQueryString(".son_form_form_name", qs.formName);
    }

    function populateFieldFromQueryString(selector, value) {
        if (typeof value != 'undefined') {
            $(selector).val(value);

            if (selector == ".son_form_optin_email" || selector == ".son_form_optin_sms" || selector == ".son_form_ht_conducted")
                $(selector).change();
        }
    }

}) ()

function parseQueryString() {
    var queryString = window.location.search.substring(1);
    var query = {};
    var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return query;
}
(function () {
    var $input = $(".j-predictive-location-input").find("input");
    var dropdownOutput = $(".j-predictive-location-output");
    var $outputSelect = dropdownOutput.find("select");

    // disable on load
    //$outputSelect.attr('disabled', 'disabled');

    $outputSelect.change(function () {
        var hiddenStoreCode = dropdownOutput.find('#HiddenStoreCode');
        hiddenStoreCode.val($(this).val());
    });

    /*
    $input.blur(function () {
        var request = {
            Zip: $input.val()
        };
        if ($input.val().length > 0) {
            // ajax get stores from handler if PLZ inserted
            $.post("/CMSPages/Sonova/LocationListHandler.ashx", JSON.stringify(request), function (data) {
                var out = data;
                $outputSelect.empty();
                $.each(out.data, function (val, text) {
                    $outputSelect.append(
                        $('<option></option>').val(text.Value).html(text.Name)
                    );
                });
                // again enable if something exists or disable it
                if (out.data.length > 1) {
                    $outputSelect.removeAttr('disabled');
                } else {
                    $outputSelect.attr('disabled', 'disabled');
                }
            });
        }
    });
    */

    $input.keyup(function () {
        var request = {
            Zip: $input.val(),
            DisplayFormat: $('#GeoDisplayValue').val(),
            ValueFormat: $('#GeoValue').val(),
            customUrlParameters: $('#GeoCustomUrlParametersValue').val()
        };
        if ($input.val().length > 3) {
            // ajax get stores from handler if PLZ inserted
            $.post("/CMSPages/Sonova/LocationListHandler.ashx", JSON.stringify(request), function (data) {
                var out = data;
                $outputSelect.empty();
                $outputSelect.attr('disabled', 'disabled');
                $.each(out.data, function (val, text) {
                    $outputSelect.append(
                        $('<option></option>').val(text.Value).html(text.Name)
                    );
                });
                // again enable if something exists or disable it
                if (out.data.length > 1) {
                    $outputSelect.removeAttr('disabled');
                } else {
                    $outputSelect.attr('disabled', 'disabled');
                }
            });
        }
    });

})();
// Accordion
(function () {
    var $accordion = document.querySelectorAll(".j-accordion .toggle");

    for (var i = 0; i < $accordion.length; i++) {

        var _this = $accordion[i];
        var acoParent = $accordion[i].parentElement;

        _this.addEventListener("click", function () {
            toggleAccordion(this);
        }, false);
    }

    function toggleAccordion(_current) {
        var currentBlock = _current.parentNode;
        var multiple = _current.parentNode.classList.contains("multiple"); // Default settings for accordion

        var isExpanded = _current.parentNode.classList.contains("expanded");

        if (multiple) { 
            currentBlock.classList.toggle("expanded");
        } else if (!isExpanded) {
            for (var i = 0; i < $accordion.length; i++) {
                $accordion[i].parentNode.classList.remove("expanded");
            }
            currentBlock.classList.add("expanded");
        } else {
            currentBlock.classList.remove("expanded");
        }
    }
})();
// appointmenttypeselection
(function () {
    function getSelectedValue(element) {
        if (element.dataset.url && element.dataset.value) {
            return {
                url: element.dataset.url,
                value: element.dataset.value,
            };
        }

        return undefined;
    }

    function setCookie(val) {
        var d = new Date();
        d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = 'appointment-type=' + val + ';' + expires + ';path=/';
    }

    var appointmentTypeButtons = document.querySelectorAll(".j-appointment-type-button");
    // IE needs this, because querySelectorAll does not return an array
    appointmentTypeButtons = Array.prototype.slice.call(appointmentTypeButtons);

    appointmentTypeButtons.forEach(function (elem) {
        elem.addEventListener("click", function (ev) {
            // click can be in the label or button
            var elem = ev.currentTarget.querySelector('.j-appointment-type-option')
            var val = getSelectedValue(elem ? elem : ev.target);

            if (typeof val === 'undefined') {
                setCookie('');
            } else {
                setCookie(val.value);
                document.location.href = val.url;
            }
            ev.preventDefault();
            return false;
        }, false);
    });
})();
(function () {

    /* Comparator global vars */
    var $comparatorSlider = $(".j-product-slider-comparator");
    var wantedItem = $comparatorSlider.find(".j-item-wanted").index();

    /******************
        Progress bar
    *******************/
    var $progressIcon = $comparatorSlider.find(".j-progress-value");
    var radius;
    var circumference;

    if ($progressIcon.length > 0) {
        radius = $progressIcon[0].r.baseVal.value;
        circumference = radius * 2 * Math.PI;

        $progressIcon.css("stroke-dasharray", circumference + ", " + circumference);
        $progressIcon.css("stroke-dashoffset", circumference);
    }

    // Function set progress
    function setProgress(percent, i) {
        var offset = circumference - percent / 100 * circumference;
        $progressIcon.eq(i).css("stroke-dashoffset", offset);
    }

    // Set value
    if ($progressIcon.length > 0) {
        $progressIcon.each(function (i) {
            var $that = $progressIcon.eq(i);
            var value = $that.attr("data-percent");

            if (value > 0 && value <= 100) {
                setProgress(value, i);
            }
        });
    }
        


    /************************
        Slider Comparator
    ************************/

    // Init slideru
    $comparatorSlider.owlCarousel({
        items: 1.2,
        loop: false,
        nav: false,
        dots: true,
        center: true,
        margin: 15,
        startPosition: wantedItem,
        responsive: {
            1920: { items: 7.2 },
            1600: { items: 6 },
            1440: { items: 5.8 },
            1280: { items: 4.5 },
            1170: { items: 4.5 },
            992: { items: 4 },
            960: { items: 3.5 },
            800: { items: 3 },
            768: { items: 2.6 },
            600: { items: 2.5 },
            520: { items: 2.25 },
            480: { items: 2 },
            380: { items: 1.5 },
            0: { items: 1.3 }
        }
    });

})();
(function () {
    /* Category selector */
    $('.j-product-selector-carousel').owlCarousel({
        loop: false,
        margin: 10,
        responsiveClass: true,
        nav: true,
        navText: ["<svg xmlns='http://www.w3.org/2000/svg' class='icon icon-btn-arrow' viewBox='0 0 20 20'><path d='M6.8 9.2L12.2 6l.4-.1c.3 0 .5.1.7.4.2.4.1.9-.3 1.1L8.7 10l4.3 2.5c.4.2.5.7.3 1.1-.2.4-.7.5-1.1.3l-5.4-3.2s-.4-.2-.4-.7c0-.6.4-.8.4-.8z'/></svg>",
            "<svg xmlns='http://www.w3.org/2000/svg' class='icon icon-btn-arrow' viewBox='0 0 20 20'><path d='M13 10.7l-5.4 3.2-.4.1c-.3 0-.5-.1-.7-.4-.2-.4-.1-.9.3-1.1l4.3-2.6-4.3-2.5c-.4-.2-.5-.7-.3-1.1.2-.4.7-.5 1.1-.3L13 9.2s.4.2.4.7c0 .6-.4.8-.4.8z'/></svg>"],
        dots: false,
        responsive: {
            0: {
                items: 3,
            },
            420: {
                items: 4,
            },
            500: {
                items: 4,
            },
            700: {
                items: 4,
            },
            1000: {
                items: 5,
            }
        }
    });

    /* Product Gallery */
    $('.j-product-info-carousel').owlCarousel({
        loop: false,
        margin: 10,
        items: 1,
        nav: false,
        dots: true
    });

    /* Partners Gallery */
    $('.j-partners-carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        dots: false,
        center: true,
        stagePadding: 50,
        nav: false,
        responsive: {
            0: {
                margin: 10,
                items: 1.5
            },
            500: {
                margin: 20,
                items: 5
            },
            1000: {
                items: 7,
                margin: 30
            }
        }
    });
})();
(function () {
    // Fullwidth carousel with stagePadding
    $(".j-slider-articles-full-size").owlCarousel({
        nav: false,
        dots: true,
        dotsEach: true,
        loop: true,
        margin: 16,
        responsive: {
            0: { items: 1, stagePadding: 16 },
            768: { items: 2, stagePadding: 16 },
            1100: { items: 2, stagePadding: 48 },
            1240: { items: 2, stagePadding: 96 },
            1280: { items: 2, stagePadding: 128 },
            1366: { items: 2, stagePadding: 160 },
            1440: { items: 2, stagePadding: 204 },
            1536: { items: 2, stagePadding: 252 },
            1600: { items: 2, stagePadding: 286 },
            1680: { items: 2, stagePadding: 324 },
            1920: { items: 2, stagePadding: 436 }
        }
    });

    // Normal Grid size carousel
    $(".j-slider-articles").owlCarousel({
        nav: false,
        dots: true,
        dotsEach: true,
        loop: true,
        margin: 16,
        responsive: {
            0: { items: 1 },
            768: { items: 2 }
        }
    });
})();
(function () {
    var $signUpBox = $(".j-signup-box");
    var $tabs = $signUpBox.find(".j-tab");
    var $content = $signUpBox.find(".j-content");

    // Event Listeners
    $tabs.on("click", function (event) {
        $that = $(this);
        target = $($that.attr("href"));
        // Change active class to event target
        $tabs.removeClass("active");
        $that.addClass("active");

        // Hide all and open targeted
        $content.children().hide(0).removeClass("active");
        target.show(0).addClass("active");

        event.stopPropagation();
        return false;
    });
})();
/* Contact Popup */
(function () {

    // Vars
    var $contactButton = $(".j-contact-popup-open");
    var $contactButtonContainer = $(".j-contact-button");
    var $contactContent = $(".j-contact-popup-content");
    var $contactButtonClose = $(".j-contact-popup-close");
    var $contactAnimWrapper = $(".j-contact-anim-wrapper");
    var transitionTime = 250;
    var transitionTimeClose = 150;
    var bodyElem = $("body");

    // Hiding and showing vars
    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
        
    // Open contact
    function openContactPopup() {
        $contactAnimWrapper.addClass("active");
        setTimeout(function () {
            $contactContent.addClass("active");
            bodyElem.addClass("contact-opened");
        }, transitionTime);
    }

    // Close contact
    function closeContactPopup() {
        $contactContent.removeClass("active");
        setTimeout(function () {
            $contactAnimWrapper.removeClass("active");
            bodyElem.removeClass("contact-opened");
        }, transitionTimeClose);
    }

    // Show contact button if scrolling up
    function hasScrolled() {
        var st = $(this).scrollTop();

        // Make sure they scroll more than delta
        if (Math.abs(lastScrollTop - st) <= delta)
            return;

        if (st > 700) {
            $contactButtonContainer.addClass('contact-shown');
            $contactAnimWrapper.addClass('contact-shown');
        } else {
            $contactButtonContainer.removeClass('contact-shown');
            $contactAnimWrapper.removeClass('contact-shown');
        }

        lastScrollTop = st;
    }

    // Listeners
    $contactButton.click(function () {
        openContactPopup();
    });
    $contactButtonClose.click(function () {
        closeContactPopup();
    });

    // Scrolling listener
    $(window).scroll(function (event) {
        didScroll = true;
    });
    setInterval(function () {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

})();
/* Boots Filter tabs */
(function () {
  var $container = $(".j-filter-tabs");
  var $tabs = $container.find(".j-tab");
  var $content = $container.find(".j-content");
  var $tables = $content.find(".j-table");
  var canSwitch = true; // useful delay to avoid visual artefuckt and let behavior fully animated :>

  // Mobile selectmenu
  var $mobileSwitchDropdown = $container.find(".j-filter-table");
  console.log($mobileSwitchDropdown);

  // Re-render height + show tables
  function renderTabsHeight() {
    /* check if horizontal version */
    if (window.innerWidth >= 1200) {
      $tabs.animate(
        {
          height: $content.height() + 48 + "px"
        },
        50,
        "swing",
        function () {

          setTimeout(
            function () {
              $content.animate(
                {
                  opacity: 1
                },
                100
              );
              if ($content.hasClass("tab-1")) {
                $content.removeClass("tab-1");
              }
            }
            , 100);
        }
      );
    } else {
      $content.animate(
        {
          opacity: 1
        },
        100
      );
    }
  }

  // change table function
  function changeTable(newTable) {
    //console.log("Old height: " + $content.height());
    $tables.css("display", "none");

    if (newTable !== "all") {
      $('[data-table=' + newTable + ']').css("display", "table");
    } else {
      $tables.css("display", "table");
    }
    renderTabsHeight();
  }

  /* Initial */
  renderTabsHeight();

  /* Event listeners */
  $tabs.on("click", function () {
    // Check if possible switch tabs
    if (!canSwitch || $(this).hasClass("active")) {
      return false;
    } else {
      // turn of temporary switch
      var $that = $(this);
      var newFilter = $that.data("filter");
      canSwitch = false;

      $content.animate(
        {
          opacity: 0
        },
        100,
        // call filter
        changeTable(newFilter)
      );

      // set element active
      $tabs.removeClass("active");
      $that.addClass("active");

      setTimeout(function () {
        canSwitch = true;
      }, 350);
    }
  });

  $mobileSwitchDropdown.change(function () {
    var selected = $(this).val();
    changeTable(selected);
  });

  function showNumberOfAids(number) {
    $('td.j-tableItem, th.j-tableItem').each(function () {
      var $this = $(this);
      $this.text($this.data('text' + number));
      if ($this.data('column' + number)) {
        $this.attr('data-column', $this.data('column' + number));
      }
    });
  }
  function filterBrands(brand) {
    if (brand === 'all') {
      $('tr.j-tableRow').show();
    } else {
      $('tr.j-tableRow').hide();
      $('tr.j-tableRow.j-brand-' + brand).show();
    }
    renderTabsHeight();
  }
  function sortTable(parent, dataField) {
    var $parent = $(parent);
    var items = $parent.children("tr.j-tableRow")
    items.sort(function (a, b) {
      return $(a).data(dataField) > $(b).data(dataField) ? 1 : -1;
    });
    items.detach().appendTo($parent);
  }
  function sortTables(dataField) {
    $("table.j-table tbody").each(function () {
      sortTable(this, dataField);
    });
  }
  window.tableFilter = {
    showNumberOfAids: showNumberOfAids,
    filterBrands: filterBrands,
    sortTables: sortTables
  };
})();
// Store locator widget
(function() {
	var ENTER = 13;

	var storeLocator = $('section.j-storeLocator');
	var textInput = $(storeLocator).find("input[type='text']");
	var submitInput = $(storeLocator).find("input[type='submit']");
	var redirectUrl = $(storeLocator).find("input[type='hidden']").val();

	$(textInput).keyup(function(e) {
		var code = e.which;
		
		if (code == ENTER)
			redirect();
	});

	$(submitInput).click(function(e) {
		e.preventDefault();
		redirect();
	});

	function redirect() {
		var searchText = $(textInput).val();
		var fullRedirectUrl = redirectUrl.format(searchText);

		window.location = fullRedirectUrl;
	}

	String.prototype.format = function () {
		var a = this;
	
		for (var k in arguments) {
			if (arguments.hasOwnProperty(k)) {
				a = a.replace(new RegExp("\\{" + k + "\\}", 'g'), arguments[k]);
			}
		}

		return a;
	}
})();
// Video Popup
(function () {
    $('.j-video-popup').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
})();
/*
    To maintain the anchor title visible when loaded from link is external 
    https://jira.wunderman.ch/browse/SON-76
*/

$(function () {
    // Only set the timer if you have a hash
    if (window.location.hash) {
        setTimeout(delayedFragmentTargetOffset, 50);
    }
});

function delayedFragmentTargetOffset() {
    var offset = $(':target').offset();
    if (offset) {
        // only screen and (max-width: 767px)
        var isMobile = window.matchMedia("only screen and (max-width: 767px)").matches;
        var scrollto = offset.top - (isMobile ? 70 : 110); // minus fixed header height
        $('html, body').animate({ scrollTop: scrollto }, 0);
    }
};
// end $(document).ready()
});
// Dropdown button - show and hide dropdown menu logic
$(document).click(function (event) {
    // IE 11 workaround
    var matches = event.target.matches ? event.target.matches('.sn-dropdown-btn') : event.target.msMatchesSelector('.sn-dropdown-btn');
    if (matches) {
        var element = $('#' + $(event.target).attr('data-dropdown-id'));
        if (element.hasClass('dropdown-btn-content-show')) {
            element.removeClass('dropdown-btn-content-show'); 
        } else {
            element.addClass('dropdown-btn-content-show'); 
        }

        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();
        return false;
    } else {
        $('.dropdown-btn-content').each(function() {
            if ($(this).hasClass('dropdown-btn-content-show')) {
                $(this).removeClass('dropdown-btn-content-show');
            }
        });
    }
});